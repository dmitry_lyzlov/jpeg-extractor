#include <QCoreApplication>

#include <QDebug>
#include <QFile>
#include <QString>
#include <QByteArray>
#include <QByteArrayMatcher>

const char g_jpegFileStartRaw[] = {0xFF, 0xD8};
const char g_jpegFileEndRaw[] = {0xFF, 0xD8};

const QByteArrayMatcher g_jpegFileStart(QByteArray::fromRawData(g_jpegFileStartRaw, sizeof(g_jpegFileStartRaw)));
const QByteArrayMatcher g_jpegFileEnd(QByteArray::fromRawData(g_jpegFileEndRaw, sizeof(g_jpegFileEndRaw)));
const int g_blockSize = 512;

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    QFile file("/home/partizan/flash-restore/good-flash.img");
    if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        qDebug() << "Can't open file " << file.fileName();
        return 1;
    }

//    if (!file.seek(0))
//    {
//        qDebug() << "Can't seek in file";
//        return 1;
//    }

    int jpegStart = -1;
    int jpegEnd = -1;
    QByteArray buffer;
    buffer.reserve(g_blockSize);
    while (!file.atEnd())
    {
        buffer = file.read(g_blockSize);
        QString str = QString("bytes = %1%2 %3%4")
            .arg(buffer.at(0))
            .arg(buffer.at(1))
            .arg(buffer.at(2))
            .arg(buffer.at(3));
        qDebug() << str;
        qDebug() << buffer;
        if ((jpegStart = g_jpegFileStart.indexIn(buffer)) != -1)
        {
            qDebug() << "Found!!!!!!!!!!!";
            break;
        }

    }
    file.close();

    if (jpegStart != -1)
    {
        qDebug() << "Not found =(";
    }

//    return a.exec();
    return 0;
}
